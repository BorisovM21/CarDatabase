package CarDatabase.db;

public enum OperationResult {
    SUCCESS,
    ERROR,
    EXIST,
    NOT_EXIST
}
