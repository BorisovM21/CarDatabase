package CarDatabase.db.memory;

import CarDatabase.db.DataBase;
import CarDatabase.db.OperationResult;
import CarDatabase.entities.Car;
import CarDatabase.entities.CarNumber;

import java.awt.*;
import java.io.Serializable;
import java.util.*;

public class MemoryDataBase implements DataBase,Serializable {

    private Map<CarNumber, Car> _data;

    public MemoryDataBase() {
        _data = new HashMap<>();
    }

    @Override
    public Map<CarNumber, Car> findAll() {

        return Collections.unmodifiableMap(_data);//TODO Запретить изменения автомобилей внутри базы
    }

    @Override
    public Car findByCarNumber(CarNumber carNumber) {
        Car modifiableCar = _data.get(carNumber);
        return Car.unmodifiable(modifiableCar);
    }

    @Override
    public OperationResult add(CarNumber carNumber, Car car) {
        Car result = _data.putIfAbsent(carNumber, car);
        if (result == null) {
            return OperationResult.SUCCESS;
        }
        return OperationResult.EXIST;
    }

    @Override
    public OperationResult delete(CarNumber carNumber) {
        Car removedCar = _data.remove(carNumber);
        if (removedCar == null) {
            return OperationResult.NOT_EXIST;
        }
        return OperationResult.SUCCESS;
    }

    @Override
    public OperationResult delete(Car car) {
        Set<CarNumber> carNumbers = new HashSet<>();
        for (Map.Entry<CarNumber, Car> entry: _data.entrySet()) {
            if (Objects.equals(entry.getValue(), car)) {
                carNumbers.add(entry.getKey());
            }
        }

        for(CarNumber carNumber: carNumbers) {
            _data.remove(carNumber);
        }


//        int oldSize = _data.size();
//        _data.entrySet().stream()
//                        .filter(entry -> Objects.equals(entry.getValue(), car))
//                        .map(entry -> entry.getKey())
//                        .forEach(key -> _data.remove(key));


        return carNumbers.size() == 0 ?
                OperationResult.NOT_EXIST :
                OperationResult.SUCCESS;
//        return oldSize == _data.size() ?
//                OperationResult.NOT_EXIST :
//                OperationResult.SUCCESS;
    }

    @Override
    public OperationResult deleteAll() {
        if (_data.size() == 0) {
            return OperationResult.NOT_EXIST;
        }
        _data.clear();

        return OperationResult.SUCCESS;
    }

    @Override
    public OperationResult setNewColor(CarNumber carNumber, Color newColor) {
        Car car = _data.get(carNumber);

        if (car == null) {
            return OperationResult.NOT_EXIST;
        }

        car.setColor(newColor);

        return OperationResult.SUCCESS;
    }
}
