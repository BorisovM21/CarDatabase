package CarDatabase.entities;


import java.io.Serializable;

public class Driver implements Cloneable,Serializable {
    private String _name;
    private String _lastname;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Driver driver = (Driver) o;

        if (_years != driver._years) return false;
        if (_name != null ? !_name.equals(driver._name) : driver._name != null) return false;
        return _lastname != null ? _lastname.equals(driver._lastname) : driver._lastname == null;
    }

    @Override
    public int hashCode() {
        int result = _name != null ? _name.hashCode() : 0;
        result = 31 * result + (_lastname != null ? _lastname.hashCode() : 0);
        return result;
    }

    public void set_years(int _years) {

        this._years = _years;
    }

    public String get_name() {

        return _name;
    }

    public String get_lastname() {
        return _lastname;
    }

    public int get_years() {
        return _years;
    }

    public Driver(String _name, String _lastname, int _years) {

        this._name = _name;
        this._lastname = _lastname;
        this._years = _years;
    }

    private int _years;



}
