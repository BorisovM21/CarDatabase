package CarDatabase;

import CarDatabase.db.DataBase;
import CarDatabase.db.memory.MemoryDataBase;
import CarDatabase.entities.Car;
import CarDatabase.entities.CarNumber;

import java.awt.*;
import java.io.*;
import java.util.GregorianCalendar;

public class Main {

    public static void main(String[] args) {

        DataBase dataBase = new MemoryDataBase();
        Car car = new Car("","", new GregorianCalendar(4,2,2), Color.BLACK);
        dataBase.add(new CarNumber("AA", 2546, "BB"), car);

        System.out.println(dataBase.delete(car).name());
        System.out.println(dataBase.delete(car).name());
        //fill

        CarNumber carNumber = new CarNumber("AA", 2546, "BB");
//        Car car = dataBase.findByCarNumber(carNumber);
        car.setColor(Color.BLACK);
        Car car1 = new Car("Mersedes", "BENZ", new GregorianCalendar(2014, 3, 21), Color.BLACK);
        CarNumber carNumber1 = new CarNumber("AA", 1547, "AE");
        dataBase.add(carNumber1, car1);

        Car car2 = new Car("Opel", "VECTRA", new GregorianCalendar(1996, 2, 1), Color.BLUE);
        CarNumber carNumber2 = new CarNumber("AA", 2211, "AB");
        dataBase.add(carNumber2, car2);

        Car car3 = new Car("Lada", "2110", new GregorianCalendar(2010, 4, 13), Color.WHITE);
        CarNumber carNumber3 = new CarNumber("AE", 1111, "AE");
        dataBase.add(carNumber3, car3);

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("carbase.dat"))) {
            oos.writeObject(dataBase);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("carbase.dat"))) {
            DataBase d = (DataBase) ois.readObject();
            System.out.println(d);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
